# OpenShift Build Infrastructure

This project contains installation scripts of the complete build infrastructure
required for developing and building applications on Openshift.

The build infrastructure includes:
* Gitlab Runner
* Nexus

## Gitlab Runner

[GitLab Runner](https://docs.gitlab.com/runner/) is an application that runs CI scripts for projects hosted 
on gitlab.com using Gitlab CI. For deployments on Kubernetes/Openshift, 
the [Kubernetes executor](https://docs.gitlab.com/runner/install/kubernetes.html) type of runner is used.
The runner application runs as a Kubernetes pod that afterwards creates dynamically
an another Pod for each CI job of the build pipeline (allowing to scale horizontally on demand).

Important configuration properties (defined in _group_vars/all.yml_):
* __gitlab_runner.gitlab_url__ - Should be set to "https://gitlab.com" for projects hosted on the public gitlab
  instance in cloud.
* __gitlab_runner.registration_token__ - This token binds the runner to a concrete project or a group. 
  In our case it should be a group token meaning the runner will build all projects in the group.
* __gitlab_runner.concurrent_jobs__ - Defines how many jobs can be built in parallel (how many building pods
  can the runner start). The number here should be set with regard to the physical limits of the cluster
  (how many nodes in the cluster are assigned for the gitlab runner itself).
* __gitlab_runner.version__ - [Image](https://hub.docker.com/r/gitlab/gitlab-runner/) version to deploy. 
  Intentionally set by default to "latest" because there is a new (minor) release once per month and also for the reason
  that Gitlab Runner needs to stay compatible with "gitlab.com" where updates occur silently on a monthly basis. 

You can get the values of these properties from your Gitlab project/group settings.
See more information on how to connect the runner with the project/group here: 
* [Registering a specific Runner with a project registration token](https://docs.gitlab.com/ee/ci/runners/#registering-a-specific-runner-with-a-project-registration-token)    

For a complete list of configuration options, see:
* [Advanced configuration](https://docs.gitlab.com/runner/configuration/advanced-configuration.html)
* [Kubernetes executor](https://docs.gitlab.com/runner/executors/kubernetes.html)

The current deployment scripts were inspired by the following resources:
* https://gitlab.com/gitlab-org/omnibus-gitlab/blob/f04b5c4443b3a98011577cdd7c9de766a287531e/docker/openshift/runner-template.yml
* https://github.com/debianmaster/openshift-examples/blob/master/gitlab-ci-with-openshift/gitlab-runner.yaml

## Nexus

Nexus is a an application that serves as a repository manager (in our case mainly as a Maven repository).
Nexus is deployed as a standalone Kubernetes deployment that requires a persistent volume 
(needs to be provided by the cluster).

Important configuration properties (defined in _group_vars/all.yml_):
* __nexus.volume_capacity__ - volume space available
* __nexus.max_memory__ - max memory allocated 

The current deployment scripts were inspired by the following resources:
* https://github.com/OpenShiftDemos/nexus/blob/master/nexus3-persistent-template.yaml
* https://github.com/sonatype-nexus-community/deployment-reference-architecture/blob/master/OpenShift/nexus-repository-manager.yaml

## Deploy the build infrastructure

1. Before executing the deployment scripts, the following dependencies have to be installed on the machine,
where the Ansible playbook will be executed:
    * `pip install openshift`
1. Start the deployment of the complete Openshift build infrastructure project as follows:
    * `ansible-playbook -i <inventory_name> install-build-infra.yml --vault-id=<path-to-pwd-file>`
    * where _<inventory_name>_ is the target environment (i.e. "dev", "prod", etc.),
    * where _vault-id_ parameter requires a path to file containing the Vault password (not pushed in the repository), 
1. As an Openshift cluster admin, execute the following command:
   * `oc adm policy add-scc-to-user privileged -z gitlab-runner -n build-infra`
     * This is required for correct run of Gitlab Runner which requires to run in "privileged" mode.
     * Normally, only your cluster administator (operations department) can do it.
     * It is required to run it only once - the settings will survive a subsequent project deletion and recreation.

## Local development

It is possible to deploy and test the build infrastructure locally on
[Minishift](https://github.com/minishift/minishift).

Follow these steps:
1. [Install and start minishift](https://docs.okd.io/latest/minishift/getting-started/index.html).
1. Obtain the minishift cluster IP:
   * `minisfhit ip`
1. Login as a developer (default pwd = "developer"):
   *  `oc login -u developer`
1. You should be now logged in as a _developer_ user. Obtain its login token: 
    * `oc whoami -t`
1. Deploy the build infrastructure (replace minishift IP, developer token and vault file path):
    * `ansible-playbook -i local install-build-infra.yml --vault-id=<path-to-pwd-file> -e "os_cluster_url=https://<cluster-ip>:8443 os_username=developer os_auth_token=<developer-token>"`
1. Login as administrator: 
   * `oc login -u system:admin`
1. Enable "privileged" access for "gitlab-runner" user:
   * `oc adm policy add-scc-to-user privileged -z gitlab-runner -n build-infra`
   
If everything went well, the build infrastructure should be deployed and ready to use.

### Test Gitlab Runner locally 

To verify the gitlab runner you have just installed is working, you would need to connect it with a project
hosted on Gitlab. To do that:
1. Open the _Settings->CI/CD->Runners_ page of your Gitlab project and copy the registration token
   of a specific runner under _Set up specific Runner manually_ section.
1. Define the _gitlab_runner.registration_token_ in _group_vars/local/vars.yml_ with the value
   of the token from the previous step.
   * Do not push this to the Git Repository.
1. Redeploy again the build infrastructure (at least the gitlab runner):
   * `ansible-playbook -i local install-build-infra.yml --vault-id=<path-to-pwd-file> -e "os_cluster_url=https://<cluster-ip>:8443 os_username=developer os_auth_token=<developer-token>"`
1. Trigger a build of your project on gitlab.com
   * The build should now execute successfully using the local Gitlab Runner instance (you can check the pod logs).

### Test Nexus locally

1. Just navigate to the Openshift console and open the _Applications->Routes_ menu. If the deployment was successful, 
   you should see here an URL to your locally installed Nexus instance. 

## TODO

* Start Gitlab Runner in an unprivileged mode.
* Deploy multiple runner instances (3-5?) in parallel to improve stability.
* If running in privileged mode: For a better security, deploy Gitlab Runner on specific cluster nodes only (for example labeled as "ci-node")
  and all the other applications on different nodes. This will limit the "privileged scope" only to specific
  nodes in cluster.
* Operations team: Assign "system:deployer" role to Gitlab Runner service account (to be able to deploy in every project).  
* Enable caching.
* Set optimal "concurrent" job count (depends on assigned physical resources).